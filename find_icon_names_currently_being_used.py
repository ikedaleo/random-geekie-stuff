import os
import re
import glob
from pprint import pprint

# Define the directory to search in
search_dir = '../../rn/'  # Replace with your actual codebase path

# Define the list to store results
captured_names = []
string_names_by_path = {}
braces_names_by_path = {}

# Define the list of removed icon names (obtained from get_icons_removed_between_versions.py)
removed_icons = ['md-eye', 'md-arrow-back', 'md-cafe', 'md-checkmark-circle', 'md-planet', 'md-trash', 'md-arrow-dropdown', 'md-send', 'md-open', 'ios-refresh', 'md-hand', 'md-close-circle', 'ios-paper']  # Replace with your actual list

# Find all .jsx and .tsx files
file_paths = glob.glob(os.path.join(search_dir, '**/*.jsx'), recursive=True) + \
             glob.glob(os.path.join(search_dir, '**/*.tsx'), recursive=True)

# Regular expressions to match import statements and JSX elements
jsx_element_regex = re.compile(r"<(\w+)")
name_string_prop_regex = re.compile(r"name=\"[^\"]*\"|name='[^']*'")
name_braces_prop_regex = re.compile(r"name=\{[^}]*\}")
# !!! Swap Ionicons with icon set to be checked (MaterialCommunityIcons, MaterialIcons, etc)
import_regex = re.compile(r"import\s+(\w+)\s+from\s+'react-native-vector-icons/Ionicons';")

# Iterate over all found files
for file_path in file_paths:
    with open(file_path, 'r', encoding='utf-8') as file:
        content = file.read()
        # Check if the MaterialCommunityIcons set is being imported
        import_match = import_regex.search(content)
        if import_match:
            given_name = import_match.group(1)
            # Find all occurrences of <GivenName
            jsx_matches = jsx_element_regex.finditer(content)
            i = 0
            for jsx_match in jsx_matches:
                if jsx_match.group(1) == given_name:
                    # Find the closest occurrence of name={.*} after the found occurrence
                    search_start = jsx_match.end()
                    name_string_match = name_string_prop_regex.search(content, pos=search_start)
                    name_braces_match = name_braces_prop_regex.search(content, pos=search_start)

                    if name_string_match:
                        captured_names.append(name_string_match.group())
                        string_names_by_path[file_path + f"_{i}"] = name_string_match.group()
                        i += 1

                    if name_braces_match:
                        captured_names.append(name_braces_match.group())
                        braces_names_by_path[file_path + f"_{i}"] = name_braces_match.group()
                        i += 1

print("================= DONE PARSING CODEBASE =======================")
pprint(string_names_by_path)
pprint(braces_names_by_path)
print("================= ^ RAW MATCHES ^ =======================")

def extract_quoted_substrings(strings):
    # Regular expressions to match ".*" and '.*'
    double_quote_regex = re.compile(r'"[^"]*"')
    single_quote_regex = re.compile(r"'[^']*'")

    # Array to store found matches
    found_matches = []

    for string in strings:
        # Find all matches for both regex patterns
        double_quote_matches = double_quote_regex.findall(string)
        single_quote_matches = single_quote_regex.findall(string)

        # Remove the surrounding quotes and add the matches to the array
        found_matches.extend([match[1:-1] for match in double_quote_matches])
        found_matches.extend([match[1:-1] for match in single_quote_matches])

    return found_matches


captured_names = extract_quoted_substrings(captured_names)

print("================= V NAMES BEING USED V =======================")
for name in sorted(captured_names):
    print(name)
print("================= ^ NAMES BEING USED ^ =======================")

def find_intersection(list1, list2):
    set1 = set(list1)
    set2 = set(list2)
    intersection = set1.intersection(set2)
    return list(intersection)

inter = find_intersection(removed_icons, captured_names)
print("FINAL RESULTS: REPLACE THE FOLLOWING")
print("(Remember to manually check cases where a variable is passed to name (braces_names_by_path))")
print(inter)
