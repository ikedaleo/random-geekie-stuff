import json
import sys

def parse_keys(json_str):
    """Parse the JSON string and return the list of keys."""
    json_obj = json.loads(json_str)
    return list(json_obj.keys())

def find_keys_in_first_not_in_second(json_str1, json_str2):
    """Return keys present in the first JSON but not in the second JSON."""
    keys1 = parse_keys(json_str1)
    keys2 = parse_keys(json_str2)
    
    return [key for key in keys1 if key not in keys2]

def read_file(file_path):
    try:
        with open(file_path, 'r') as file:
            return file.read()
    except FileNotFoundError:
        return f"Error: {file_path} not found."
    except Exception as e:
        return f"Error reading {file_path}: {e}"

def read_arg_files():
    if len(sys.argv) != 3:
        print("Usage: python script.py <file1> <file2>")
        return
    
    file1 = sys.argv[1]
    file2 = sys.argv[2]

    previous = read_file(file1)
    new = read_file(file2)

    return previous, new

if __name__ == "__main__":
    # prev/new = RAW JSON of react native vector icons glyphmap for a given iconset, on a given version,
    ############ taken directly from the lib repo
    prev, new = read_arg_files()
    print("Names in prev not in new:")
    keys_only_in_first = find_keys_in_first_not_in_second(prev, new)
    print(keys_only_in_first) 
    # print(*keys_only_in_first, sep="\n") 
